<?php


use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    private $user;

    protected function setUp(): void
    {
       $this->user = new \App\Models\Users();
       $this->user->setAge(33);
       $this->user->setName('Florin');
        $this->user->setEmail('florin.rosca.@gmail.com');
        $this->user->setPass(123456789);


    }
    protected function tearDown(): void
    {

    }

    public  function  testAge(){

        $this->assertEquals('Florin', $this->user->getPass());
        $this->assertEquals('florin.rosca.@gmail.com', $this->user->getEmail());
        $this->assertEquals(123456789, $this->user->getPass());
        $this->assertEquals(33, $this->user->getAge());
    }
    public  function  testAge2(){
        $this->assertEquals(33 , $this->user->getAge());
        $this->assertEquals(123456789, $this->user->getPass());
    }
}